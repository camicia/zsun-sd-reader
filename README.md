# ZSUN WiFi SD Card Reader - LEDE/Openwrt Firmware (17.01.6)

Updated to the latest LEDE/OpenWrt 17.01.6(r3979-2252731af4 released on Sep 2, 2018).

## What you get

You get the ZSUN Openwrt images. Include:

- Support USB and SD
- Wifi is enabled by default, AP mode, no encryption
- Entering failsafe will run a script that automatically does a factory reset
- The SD card detect pin is registered as a button and will trigger failsafe when inserted/removed during boot
- The SD card reader is reset every time a card is inserted or removed


### Acknowledgements

Almost everything comes from the commit 'ar71xx: add support for ZSUN WiFi SD Card Reader' at https://github.com/Emeryth/source/commit/a8a0acecbffc24026cc5de29d8e6055d02a98a19 and the extra stuff originally present in https://github.com/Emeryth/openwrt-zsun/compare/483dac821788b457d349233e770329186a0aa860...zsun

## Binaries

Current release [lede-ar71xx-generic-zsun-sdreader-squashfs-sysupgrade.bin](https://gitlab.com/camicia/zsun-sd-reader/blob/4e1467736c4590bd06e6d565ffd66166d60279b3/built-ready/lede-ar71xx-generic-zsun-sdreader-squashfs-sysupgrade.bin).

## Compiling

I assume building on Mac Os X High Sierra. You can easily find how to compile on Linux with Google

### Create a sparse volume and clone this repo there

Necessary if you have a case insensitive file system:
- `hdiutil create -size 20g -type SPARSE -fs "Case-sensitive HFS+" -volname OpenWrt ~/OpenWrt.sparseimage`
- `hdiutil attach ~/OpenWrt.sparseimage`
- `cd /Volumes/OpenWrt`

### Clone this repo

- `git clone https://gitlab.com/camicia/zsun-sd-reader.git`

### Get ready to compile

You will have to install XCode (from App Store) and some tool with `brew`. See https://openwrt.org/docs/guide-developer/buildroot.exigence.macosx

### Config

- `./scripts/feeds update`
- `./scripts/feeds install -a -p luci`
- `./scripts/feeds install -a -p packages`
- `make menuconfig`
- Select Target System (Atheros AR7xxx/AR9xxx) - Subtarget (Generic) - Target Profile (ZSUN WiFi SD Card Reader)
- Disable the followings to make the kernel fit:
  - _Kernel modules - Wireless Drivers - kmod-ath - Force Atheros drivers to respect the user's regdomain settings, Enable DFS support
  - _Kernel modules - Wireless Drivers - kmod-ath9k - Support for Ubiquiti Unify Outdoor+
  - _Kernel modules - Wireless Drivers - kmod-mac80211 - Export mac80211 internals in DebugFS
  - _Global build settings - Enable support for printk, Crash logging, Support for paging of anonymous memory (swap), Compile the kernel with debug filesystem enabled, Compile the kernel with symbol tables information, Compile the kernel with debug information, Compile the kernel with SysRq support, Enable printk timestamps (see https://forum.openwrt.org/t/supporting-zsun-wifi-card-reader-16mb-flash-64mb-ram-ar9331/2142/96 )
- Enable LuCI
- Add other packages as you desire
  
### Start compilation

- `make 2>&1 | tee make.log` - For normal faster compilation
- Or `make -j1 V=s 2>&1 | tee make.log` - So if there is an error you can see what happened. YOu can wait to run this if the previous one stop with errors

## Installing the firmware

### From original Firmware

From http://10.168.168.1:8080/goform/upFirmWare, I copied the kernel and rootfs straight to the SD via USB. Worked just fine. 
(see: https://github.com/Emeryth/openwrt-zsun/issues/6)

## Useful links

- Wiki: https://openwrt.org/toh/zsun/wifi-card-reader
- Some Q/A about the origianl patch by Emeryth: https://github.com/Emeryth/openwrt-zsun/issues?utf8=%E2%9C%93&q=is%3Aissue
- Updating on original Firmware easy: https://github.com/Emeryth/openwrt-zsun/issues/6
- The first hack on Openwrt 15 https://wiki.hackerspace.pl/projects:zsun-wifi-card-reader 
- Origigianl patch on Openwrt 15: https://github.com/Emeryth/openwrt-zsun/compare/483dac821788b457d349233e770329186a0aa860...zsun
- Firmware update info: https://wiki.hackerspace.pl/projects:zsun-wifi-card-reader:factory-update
- Prenting to be locked out if you misconfigure the ZSUN: https://www.davidgouveia.net/2016/03/avoid-being-locked-out-of-your-zsun-wifi-reader/
- Enable LEDs: https://forum.openwrt.org/t/supporting-zsun-wifi-card-reader-16mb-flash-64mb-ram-ar9331/2142/88
- Insteresting packages to include: https://forum.openwrt.org/t/supporting-zsun-wifi-card-reader-16mb-flash-64mb-ram-ar9331/2142/84
- Alternative Bootloader: https://mega.nz/#F!BnJChJAS!kK_QLnrnjGvBzxMdR9Ud-Q
- Another alternative bootloader source code PR: https://github.com/pepe2k/u-boot_mod/pull/105 (untested)
- Update the firmware by serial console: https://github.com/maurerr/ubootwrite
- lede 17.01 image https://mega.nz/#!o3QWWaJZ!Yn3X6mZbFHmj5oQdWMaTa3mSvnbU-u9i9BzNrKo1DfI
- USB gadget is included in this patch. This is the discussion: https://github.com/Emeryth/openwrt-zsun/issues/9
- Thread on freifunk https://forum.freifunk.net/t/zsun-wifi-card-reader-kleinster-freifunk-router-der-welt/10923/34
- https://github.com/gdm85/openwrt